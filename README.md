# BEESE 🐝 ![Minecraft: 1.12.2](https://img.shields.io/badge/minecraft-1.12.2-brightgreen.svg) ![Forge: 14.23.5.2847](https://img.shields.io/badge/forge-14.23.5.2847-red.svg)

![Server Status](https://mcapi.us/server/image?ip=167.114.3.144)

---

**IP:** `vps260819.vps.ovh.ca`

**IP (Backup):** `167.114.3.144`

**Modlist GSheet:** https://1n.pm/aadBP

---

Hosted with 💗 by Eru.
