#!/bin/sh
while true
do
  tmux send-keys -t minecraft:0 "say Restarting in 10 minutes!" C-m
  sleep 300s
  tmux send-keys -t minecraft:0 "say Restarting in 5 minutes!" C-m
  sleep 240s
  tmux send-keys -t minecraft:0 "say Restarting in 1 minute! Prepare to be booted..." C-m
  sleep 55s
  tmux send-keys -t minecraft:0 "say Restarting in 5 seconds!" C-m
  tmux send-keys -t minecraft:0 "stop" C-m
  echo "Stopped Minecraft server tmux session. Restarting..."
  sleep 86100s
done
